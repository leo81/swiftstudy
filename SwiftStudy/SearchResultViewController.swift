//
//  SearchResultViewController.swift
//  SwiftStudy
//
//  Created by BW.Park on 2018. 10. 5..
//  Copyright © 2018년 BW. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection

class SearchResultViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
	
    
    @IBOutlet weak var btnCategoryMovie: UIButton!
    @IBOutlet weak var btnCategoryBook: UIButton!
    @IBOutlet weak var btnCategoryImage: UIButton!
    @IBOutlet weak var btnCategoryBookmark: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var buttons : Array<UIButton> = []
	
	var currentSearchEngine : SearchEngine = NaverSearchEngine()
	var searchType : SearchAPIType = .Movie
	var searchText : String?
	var currentPage : Int = 1
	var fethSize : Int = 20
	
	var items : [SearchResultItem]?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttons = [btnCategoryMovie, btnCategoryBook, btnCategoryImage, btnCategoryBookmark]
    }
    
    func getSelectedSearchType() -> Int {
        for aButton in buttons {
            if aButton.isSelected {
                return aButton.tag
            }
        }
        
        // default
        return btnCategoryMovie.tag
    }
    
    @IBAction func changeCategory(_ sender: UIButton) {
        search(searchType: SearchAPIType(rawValue: self.getSelectedSearchType())!, page: 1)
    }
	
	func search(searchType : SearchAPIType, page : Int) -> Void {
        let urlString = currentSearchEngine.urlString(apiType: searchType)
		
        var params = Parameters()
        params["query"] = self.searchText
        
        SessionManager.default
            .request(urlString, method: .get, parameters: params, encoding: Alamofire.URLEncoding.default, headers: currentSearchEngine.headers)
			.responseObject { (response : DataResponse<SearchResult>) in
				//
				
				if page <= 1 {
					self.items = response.value?.items
				} else {
					//Array
					self.items = (self.items ?? []) + (response.value?.items ?? [])
				}
				
				self.tableView.reloadData()
		}
    }
	
	// MARK: - UITableViewDataSource
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell : SearchResultCell = tableView.dequeueReusableCell(withClass: SearchResultCell.self)
		
		if let item = self.items?[indexPath.row] {
			cell.setSearchResult(item: item)
		}
		
		return cell
	}
	
}
