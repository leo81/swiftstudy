//
//  SearchResultCell.swift
//  SwiftStudy
//
//  Created by BW.Park on 10/10/2018.
//  Copyright © 2018 BW. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireImage

extension UIImageView {
	func imageFromUrl(urlString: String) {
		
		Alamofire.request(urlString)
			.validate(statusCode: 200..<300)
			.responseData { (response : DataResponse<Data>) in
			self.image = Image(data: response.value!)?.af_imageAspectScaled(toFill: self.size)
		}
	}
}

class SearchResultCell : UITableViewCell {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var favorateButton: UIButton!
	@IBOutlet weak var mainImageView: UIImageView!
	@IBOutlet weak var contentLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
    }
	
	func setSearchResult(item : SearchResultItem) {
		self.titleLabel.text = item.title
		
		if let imageUrl = item.imageUrl {
			self.mainImageView.imageFromUrl(urlString: imageUrl)
		} else {
			self.mainImageView.image = nil
		}
		
		self.contentLabel.text = item.desc
		self.dateLabel.text = item.regDate
	}
}
