//
//  ViewController.swift
//  SwiftStudy
//
//  Created by BW.Park on 2018. 10. 5..
//  Copyright © 2018년 BW. All rights reserved.
//
// 네이버 검색 API
// Client ID : rgJPgcYmJEKqupYXm49O
// Client Secret : AT7l9GsZU6

import UIKit
import SwifterSwift

class ViewController: UIViewController {

	@IBOutlet weak var searchBarTopMarginConstraint: NSLayoutConstraint!
	@IBOutlet weak var searchBarLeftMarginConstraint: NSLayoutConstraint!
	@IBOutlet weak var searchBarRightMarginConstraint: NSLayoutConstraint!
	
    @IBOutlet weak var textField: UITextField!
    
    var searchResultViewController : SearchResultViewController!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
		self.searchResultViewController.view.isHidden = true;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchResultViewController" {
            self.searchResultViewController = segue.destination as! SearchResultViewController
        }
    }


    @IBAction func openSearchEngineList(_ sender: Any) {
        
    }
    
    @IBAction func doSearch(_ sender: Any) {
		UIView.animate(withDuration: 0.35, delay: 0.15, options: UIView.AnimationOptions.curveEaseIn, animations: {
			self.searchBarTopMarginConstraint.priority = 999;
			self.searchBarLeftMarginConstraint.constant = 8;
			self.searchBarRightMarginConstraint.constant = 8;
			self.view.setNeedsLayout()
			self.view.layoutIfNeeded()
			
		}) { (isFinished) in
			self.searchResultViewController?.view.isHidden = false;
		}
		
		self.searchResultViewController.searchText = self.textField.text
		self.searchResultViewController.search(searchType: .Movie, page: 1)
    }
    
    // MARK: Private
    
}

