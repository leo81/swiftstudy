//
//  Model.swift
//  SwiftStudy
//
//  Created by BW.Park on 16/10/2018.
//  Copyright © 2018 BW. All rights reserved.
//

import Foundation
import EVReflection

public class NaverMovieResult : EVNetworkingObject {
    public var lastBuildDate: String?
    public var total: Int = 0
    public var start: Int = 0
    public var display: Int = 0
    public var items: [NaverMovie]? = nil
    
//    public init(lastBuildDate: String, total: Int, start: Int, display: Int, items: [NaverMovie]) {
//        self.lastBuildDate = lastBuildDate
//        self.total = total
//        self.start = start
//        self.display = display
//        self.items = items
//    }
}

public class NaverMovie {
    public var title: String
    public var link: String
    public var description: String
    public var bloggername: String
    public var bloggerlink: String
    public var postdate: String
    
    public init(title: String, link: String, description: String, bloggername: String, bloggerlink: String, postdate: String) {
        self.title = title
        self.link = link
        self.description = description
        self.bloggername = bloggername
        self.bloggerlink = bloggerlink
        self.postdate = postdate
    }
}
