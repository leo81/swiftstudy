// To parse the JSON, add this file to your project and do:
//
//   var searchItem = try? newJSONDecoder().decode(SearchItem.self, from: jsonData)
//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseSearchItem { response in
//     if var searchItem = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire
import EVReflection

class SearchResult: EVNetworkingObject {
	var lastBuildDate: String?
	var total: Int?
	var start: Int?
	var display: Int?
	var items: [SearchResultItem]?
}

class SearchResultItem : EVObject {
	var title: String?
	var link: String?
	var desc: String?
	var imageUrl: String?
	var regDate: String?
	
	override public func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
		return [(keyInObject: "desc",keyInResource: "description")]
	}
}

class MovieItem: SearchResultItem {
	
	override public func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
		return super.propertyMapping() + [(keyInObject: "regDate",keyInResource: "postdate")]
	}
}

class BookItem: SearchResultItem {
	override public func propertyMapping() -> [(keyInObject: String?, keyInResource: String?)] {
		return super.propertyMapping() + [(keyInObject: "regDate",keyInResource: "pubdate")]
	}
}

class ImageItem: SearchResultItem {
	
}
