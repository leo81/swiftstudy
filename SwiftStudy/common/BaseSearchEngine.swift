//
//  BaseSearchEngine.swift
//  SwiftStudy
//
//  Created by ParkByungwoong on 20/10/2018.
//  Copyright © 2018 NHN Entertainment. All rights reserved.
//

import Foundation
import Alamofire

protocol SearchEngine {
	var domain: String { get }
	
	var headers : HTTPHeaders { get }
	
	func urlString(apiType : SearchAPIType) -> String
}

extension SearchEngine {
	func urlString(apiType : SearchAPIType) -> String {
		return self.domain + apiType.path
	}
}

enum SearchAPIType : Int, CaseIterable {
	case Movie = 0
	case Book
	case Image
	case Bookmark
	
	static let urlMapper: [SearchAPIType: String] = [
		.Movie : "/v1/search/movie.json",
		.Book : "/v1/search/book.json",
		.Image : "/v1/search/image",
		.Bookmark : ""
	]
	
	var path: String! {
		return SearchAPIType.urlMapper[self]!
	}
}

class NaverSearchEngine : SearchEngine {
	var domain = "https://openapi.naver.com"
	
	var headers = HTTPHeaders()
	
	init() {
		headers["X-Naver-Client-Id"] = "rgJPgcYmJEKqupYXm49O"
		headers["X-Naver-Client-Secret"] = "AT7l9GsZU6"
	}
	
	
}
